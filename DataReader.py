# This is a Library file for CSV file read and write
# Ref Link : https://docs.python.org/2/library/csv.html

import xlrd


def getDataFromSpreadsheet(fileName, sheetname):
    """ Read content from Spreadsheet identified by workbook name (file_name) and
     work sheet name (sheet_name)
    :param fileName: Workbook name (full path)
    :param sheetname: work sheet name (should be present in the workbook)
    :return: an array of column values serialized from Row# 2 till the end of the workbook.
    """
    workbook = xlrd.open_workbook(fileName)
    worksheet = workbook.sheet_by_name(sheetname)
    print worksheet
    rowEndIndex = worksheet.nrows - 1
    colEndIndex = worksheet.ncols - 1
    rowStartIndex = 1
    colStartIndex = 0
    dataRow = []

    curr_row = rowStartIndex
    while curr_row <= rowEndIndex:
        cur_col = colStartIndex
        while cur_col <= colEndIndex:
            cell_type = worksheet.cell_type(curr_row, cur_col)
            value = worksheet.cell_value(curr_row, cur_col)
            dataRow.append(value)
            cur_col += 1
        curr_row += 1
    return dataRow
