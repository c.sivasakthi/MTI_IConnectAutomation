*** Settings ***
Documentation    iConnect Report Automation
Library          Selenium2Library
Library          String
Library          Collections
Library          DataReader.py


*** Variables ***
${Dashboard}                    id=dashboard
${New_Report_Label}             xpath=//*[@id="wrapper"]/ul/li[1]/a
${Save_Report_Label}            xpath=//*[@id="wrapper"]/ul/li[2]/a
${Select_Date_Label}            xpath=//*[@id="report"]/div/div[1]/h4
${Select_Sensor_Label}          xpath=//*[@id="report"]/div/div[2]/h4
${Export_NewReport}             xpath=/html/body/div[2]/ng-view/div[1]/div[3]/div/div[5]/input[1]
${Error_Info}                   xpath=/html/body/div[2]/ng-view/div[1]/div[3]/div/div[1]
${Error_Info_close}             xpath=/html/body/div[2]/ng-view/div[1]/div[3]/div/div[1]/a/i
${Calendar_NewReport}           xpath=//*[@id="report"]/div/span/span
${Calendar_Apply_NewReport}     xpath=/html/body/div[3]/div[3]/div/button[1]
${Calendar_SavedReport}         xpath=/html/body/div[2]/ng-view/div[1]/div[3]/div/div[3]/span[2]/span
${Calendar_Apply_SavedReport}   xpath=/html/body/div[4]/div[3]/div/button[1]
${Export_SavedReport}           xpath=/html/body/div[2]/ng-view/div[1]/div[3]/div/div[6]/input
${Search_SavedReport}           xpath=//*[@id="savedReport"]/input
${Search_Content}                BD
${click_SearchContent}          xpath=//*[@id="saveView"]/ul/li/a
${Search_Content_SavedReport}   xpath=//*[@id="saveView"]/ul/li[3]/a
${Searched_Sensor}              xpath=//*[@id="treeSelectHolder"]/div
${Check_Box}                    xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/div/ul/li[1]/div/span[2]/span/input
${Check_Box_Content}            xpath=//*[@id="treeSelectHolder"]/div
${Export_Reports}               xpath=//*[@id="loadingModal"]/div/div/div/div/div


*** Keywords ***
Click on REPORTS menu header
    wait until page contains element        ${Dashboard}
    click link      Reports

Verify the Reports page has Label for each button
    wait until element is visible           ${New_Report_Label}
    ${NewReportText}   get text            ${New_Report_Label}
    log       ${NewReportText}
    ${SaveReportText}    get text        ${save_report_label}
    log       ${SaveReportText}
    wait until element is visible   ${Select_Sensor_Label}
    ${SelectDateText}   get text    ${Select_Date_Label}
    log       ${SelectDateText}
    wait until element is visible   ${Select_Sensor_Label}
    ${SelectSensorText}  get text  ${Select_Sensor_Label}
    log       ${SelectSensorText}

Verify the Export option in New Reports by selecting Date and Sensors
    wait until element is visible           ${New_Report_Label}
    wait until element is visible       ${Export_NewReport}
    click element       ${Export_NewReport}
    wait until element is visible  ${Calendar_NewReport}
    click element   ${Calendar_NewReport}
    wait until element is visible   ${Calendar_Apply_NewReport}
    click element  ${Calendar_Apply_NewReport}
    wait until element is visible  ${check_box}
    click element  ${Check_Box}
    wait until element is visible  ${Check_Box_Content}
    ${CheckBoxText}     get text    ${Check_Box_Content}
    log       ${CheckBoxText}
    wait until element is visible  ${Export_NewReport}
    click element  ${Export_NewReport}
    wait until page contains element   ${Export_Reports}
    ${ExportReports}      get text  ${Export_Reports}
    log          ${ExportReports}
    reload page

Verify the Export option in New Reports without Selecting Sensors
    wait until element is visible       ${Export_NewReport}
    click element       ${Export_NewReport}
    ${ErrorInfo}        get text    ${Error_Info}
    log           ${ErrorInfo}
    wait until element is visible  ${Error_Info_close}
    click element       ${Error_Info_close}

Verify the Export option in Saved Reports by selecting Date and Sensors
    wait until element is visible   ${save_report_label}
    click element   ${save_report_label}
    wait until element is visible  ${Calendar_SavedReport}
    click element  ${Calendar_SavedReport}
    wait until element is visible  ${Calendar_Apply_SavedReport}
    click element  ${Calendar_Apply_SavedReport}
    wait until element is visible  ${Search_SavedReport}
    input text  ${Search_SavedReport}   ${Search_Content}
    wait until element is visible   ${click_SearchContent}
    click element   ${click_SearchContent}
    wait until element is visible   ${Searched_Sensor}
    ${SearchContentText}    get text  ${Searched_Sensor}
    log   ${SearchContentText}
    wait until element is visible  ${Export_SavedReport}
    click element       ${Export_SavedReport}
    wait until element is visible  ${Export_Reports}
    ${ExportReports}      get text  ${Export_Reports}
    log           ${ExportReports}
    reload page


verify the Export option in Saved Report without Selecting Sensors
    wait until element is visible   ${save_report_label}
    click element   ${save_report_label}
    wait until element is visible       ${Export_SavedReport}
    click element       ${Export_SavedReport}
    ${ErrorInfo}        get text    ${Error_Info}
    log               ${ErrorInfo}
    wait until element is visible  ${Error_Info_close}
    click element       ${Error_Info_close}





















