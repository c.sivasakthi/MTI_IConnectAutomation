*** Settings ***
Documentation    iConnect Map page navigation with Zoom in and Zoom out operation
Library          Selenium2Library
Library          String
Library          Collections
Library          DataReader.py


*** Variables ***
${Dashboard}                id=dashboard
${Map_Header}               id=menu
${Zoom_Btns}                xpath=/html/body/ng-view/div/div/div/div[3]/div[1]/div
${Zoom_IN}                  xpath=/html/body/div[2]/ng-view/div/div[1]/div/ng-map/div/div/div/div[9]/div[1]/div/div[1]
${Zoom_Out}                 xpath=/html/body/div[2]/ng-view/div/div[1]/div/ng-map/div/div/div/div[9]/div[1]/div/div[3]


*** Keywords ***

Click on MAP menu header
    wait until page contains element        ${Dashboard}
    click link                              Map

Check the map page display and click on Zoom in and zoom out buttons
    wait until page contains element        ${Zoom_IN}
    double click element                    ${Zoom_IN}
    double click element                    ${Zoom_IN}
    wait until page contains element        ${Zoom_Out}
    double click element                    ${Zoom_Out}
    double click element                    ${Zoom_Out}
