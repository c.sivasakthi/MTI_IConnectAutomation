*** Settings ***
Documentation    iConnect machine details display in the dashboard - Filters sensor label
Library          Selenium2Library
Library          String
Library          Collections
Library          DataReader.py

*** Variables ***

${Machine1}           id=title-2
${Machine2}           id=title-3
${Machine_sensorvalues}     id=machine_2
${Machine_Name_2}     xpath=//*[@id="title-3"]
${M1_History}         id=history2
${M2_History}         xpath=/html/body/ng-view/div/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[2]/div/div/label[2]
${History_Table1}     xpath=//*[@id="dashboard"]/div[1]/div[1]/div[2]/div/div/div/div[1]/div[2]/div/table
${History_Table2}     xpath=.//*[@id='dashboard']/div[1]/div[1]/div[2]/div/div[2]
${dashboardtext}        xpath=//*[@id="dashboard"]/div[1]/div[1]/div[1]/div/div[1]
${Sensor_Class_Path}    xpath=//div[@class=contains(.," Sensors:")]
${Sensor_Btn}           id=showtemperature_0+1
${machinedetailpage}    id=/html/body/div[4]/div/div/div/div[1]
#${Sensor_xpath}         xpath=/html/body/ng-view/div/div[1]/div[1]/div[1]/div/div[2]/div[1]/div/label[2]
${sensor_status}        xpath=//*[@id="machine-2"]/a
${Sensor_xpath}         xpath=//*[@id="sensorBinding0"]/h4/b
${Sensor_Filter_class}  xpath=//div[@class=contains(.," Sensors Filtered. Verify filter.:")]
${Status_Label_Class}   xpath=//div[@class=contains(.,"Status:")]
${Healthy_class}        id=healthy_button
${Abnormal_class}       id="abnormal_button"
${Inactive_class}       id=inactive_button
${Logout_Button}        id=id-logout
${Machine_1_Click}          id=title-2
${Graph_Mac_Details}        id=sample_1
${Graph_Mac2_Details}       id=container
${Typesofsensorfilter}      xpath=//*[@id="dashboard"]/div[1]/div[1]/div[1]/div/div[2]/div[1]/div
${statusavailable}          xpath=//*[@id="dashboard"]/div[1]/div[1]/div[1]/div/div[2]/div[2]/div
${Grph_Clse_Btn}            xpath=/html/body/div[4]/div/div/div/div[1]/button
${Gr_Temp}                  xpath=.//*[@id='dashboard']/div[1]/div[1]/div[2]/div/div[1]/div/div[1]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/ul/li[2]/a
${Mac_2_Gr_Clse}            xpath=/html/body/ng-view/div/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/div[2]/div[2]/h3/span
${Mac2_Link}                xpath=.//*[@id='machine-81']/a/span
${Mac_2_Temp_Tab}           xpath=.//*[@id='dashboard']/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/ul/li[2]/a
${Mac_2_Vib_Tab}            xpath=.//*[@id='dashboard']/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[2]/ul/li[1]/a
${Grph_Mac2_Clse_Icon}      xpath=/html/body/ng-view/div/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/div[2]/div[2]/h3/span
${Gr_Chart}                 xpath=//div[@class=contains(.," highcharts-axis-labels highcharts-xaxis-labels:")]
${Dashboard_Spinner}        xpath = /html/body/ng-view/div/div[1]/div[2]/p
${Date_Picker}              xpath=/html/body/div[4]/div/div/div/div[2]/div[3]/div/div[1]/div/div[1]
${Date_Picker_Cancel_Btn}   xpath=/html/body/div[5]/div[3]/div/button[2]
${Dashboardtitle}       id=title-2
${sensorlabel1}         xpath=//*[@id="sensorBinding0"]/h4/b
${sensorvalue1}         xpath=//*[@id="machine_2"]/td[3]/div/label
${sensorlabel2}         xpath=//*[@id="sensorBinding1"]/h4/b
${sensorvalue2}         xpath=//*[@id="machine_2"]/td[4]/div/label
${sensorlabel3}         xpath=//*[@id="sensorBinding2"]/h4/b
${sensorvalue3}         xpath =//*[@id="machine_2"]/td[5]/div/label
${sensorlabel4}         xpath=//*[@id="sensorBinding3"]/h4/b
${sensorvalue4}         xpath=//*[@id="machine_2"]/td[6]/div/label
${time}                 xpath=//*[@id="machine_2"]/td[2]
${machinedetailtitle}   id=title_Main St Clow 850
${MDsensorlabel1}       xpath=//*[@id="tab-Vibration"]/a
${MDsensorlabel2}       xpath=//*[@id="tab-Vibration"]/a
${MDsensorlabel3}       xpath=//*[@id="tab-Vibration"]/a
${MDsensorlabel4}       xpath=//*[@id="tab-Temperature"]/a
${Sensorreadingslabel}  xpath=//*[@id="currentdatatable"]/tbody/tr/td[2]/span
${Reports}              xpath=//*[@id="menu"]
${selectdateid}           xpath=//*[@id="report"]/div/div[1]/h4
${Deviceid}             xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/span[3]
${Sensorid1}            xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/div/ul/li[1]/div/span[3]
${sensorid2}            xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/div/ul/li[2]/div/span[3]
${sensorid3}            xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/div/ul/li[3]/div/span[3]
${sensorid4}            xpath=//*[@id="treeView"]/ul/li/div/div/ul/li/div/div/ul/li[4]/div/span[3]
${charttab1}            xpath =//*[@id="tab-Vibration"]/a
${chartlabel}           xpath=//*[@id="highcharts-108"]/svg/text[1]
${machinedetailpageheader}  xpath=/html/body/div[4]/div/div/div/div[1]
${alertindashboard}         id=alertmessagediv
${machinedetail}        xpath=//*[@id="sample_1"]
*** Keywords ***

Check the machine names in the dashboard

    wait until page contains element       ${Machine1}
    ${Ma_Name1}     get text               ${Machine1}
    log                                    ${Ma_Name1}
    should not be equal                   ${Ma_Name1}          P/S Hyd. Pump N/W-


Check the dashboard displayed with current records of temperature and vibration

    wait until page contains                Dashboard
    wait until page contains element        ${Machine_sensorvalues}
    ${Text1}     get text                   ${Machine_sensorvalues}
    log                                     ${Text1}
    should not be empty                     ${Text1}

Click on history button and check the records display

    wait until page contains element        ${M1_History}
    click element                           ${M1_History}
    wait until page contains element        ${History_Table1}
    ${History1}     get text                ${History_Table1}
    log                                     ${History1}
    should not be empty                     ${History1}


Verify the dashboard label display

    wait until page contains element        ${dashboardtext}
    wait until element is visible           ${dashboardtext}
    wait until page contains              Dashboard
    ${dashboardtest}        get text             ${dashboardtext}
    Log                     ${dashboardtest}
    should be equal         ${dashboardtest}        Dashboard
    set test message    Dashboard label is matched

Verify the Sensor and Status filter labels in the dashboard

     wait until page contains                Sensors:
     ${filtersavailable}        get text       ${Typesofsensorfilter}
     log                       ${filtersavailable}
     wait until page contains                Status:
     ${statusavailable}         get text        ${statusavailable}
     log                         ${statusavailable}

Verify Vibration filter and check sensors are getting filtered

    wait until page contains element    ${Sensor_Btn}
    double click element                ${Sensor_Btn}
    wait until page contains            Vibration
    wait until page contains element    ${Sensor_xpath}
    double click element                ${Sensor_xpath}

Click on Temperature button and check sensors are getting filtered

    wait until page contains            Temperature
    wait until page contains            Sensors Filtered. Verify filter.
    wait until page contains element    ${Sensor_Filter_class}
    ${Text}     get text                ${Sensor_Filter_class}
    log                                 ${Text}
    double click element                ${Sensor_Btn}
    double click element                ${Sensor_xpath}

Click on status filters and check machines are getting filtered

    wait until page contains            Status:
    wait until page contains element    ${Status_Label_Class}
    wait until keyword succeeds         10s     1s       click element                ${Healthy_class}
    wait until page contains element        ${sensor_status}
    wait until keyword succeeds         10s     1s       click element                ${Abnormal_class}
    wait until page contains element        ${sensor_status}
    wait until keyword succeeds         10s     1s       click element                ${Inactive_class}
    wait until page contains element        ${sensor_status}
    wait until keyword succeeds         10s     1s       click element                ${Healthy_class}
    wait until page contains element        ${sensor_status}

Logout the iconnect application

    wait until page contains element        ${Logout_Button}
    click element                           ${Logout_Button}

Verify by clicking device link and it navigates to machine detail page

    wait until page contains element        ${dashboardtext}
    wait until element is visible           ${dashboardtext}
    wait until page contains              Dashboard
    click element                           ${Machine1}
    wait until element is visible           ${machinedetailpageheader}
    ${machinedetailpagedevicename}      get text        ${machinedetailpageheader}
    log                          ${machinedetailpagedevicename}



Verify the machine details in the graph page

    wait until page contains element            ${Graph_Mac_Details}
    ${Mac1_Details}     get text                ${Graph_Mac_Details}
    log                                         ${Mac1_Details}
    should not be empty                         ${Mac1_Details}

Verify sensor chart display

    click link                                       ${charttab1}
   # wait until page contains element                 ${chartlabel}
    click element                                    ${Grph_Clse_Btn}


Verify datepicker in graph window and check the pop-up window display

    wait until page contains element    ${Date_Picker}
    wait until element is visible       ${Date_Picker}
    click element                       ${Date_Picker}

Click on cancel button and check datepicker window get closed

    wait until page contains element    ${Date_Picker_Cancel_Btn}
    click element                       ${Date_Picker_Cancel_Btn}

Verify by clicking alert link in machine detail page
    click Link              Alert Messages
    wait until page contains element            ${alertindashboard}
    wait until element is visible               ${alertindashboard}
    ${alerts}       get text                    ${alertindashboard}
    log             ${alerts}


verify the machine details are displayed
        ${machinedetail}        get text         ${machinedetail}
        log         ${machinedetail}


Verify the dashboard has sensor labeland values for each sensor
    wait until element is visible                ${Dashboardtitle}
    wait until element is visible                ${sensorlabel1}
    ${SensorLabelText1}     get text                    ${sensorlabel1}
    log                                     ${SensorLabelText1}
    ${sensorlabelvalue1}      get text                    ${sensorvalue1}
    log                                      ${sensorlabelvalue1}
    wait until element is visible                ${sensorlabel2}
    ${SensorLabelText2}     get text                    ${sensorlabel2}
    log                                     ${SensorLabelText2}
    ${sensorlabelvalue2}      get text                    ${sensorvalue2}
    log                                      ${sensorlabelvalue2}
    wait until element is visible               ${sensorlabel3}
    ${SensorLabelText3}     get text                    ${sensorlabel3}
    log                                     ${SensorLabelText3}
    ${sensorlabelvalue3}      get text                    ${sensorvalue3}
    log                                      ${sensorlabelvalue3}
    wait until element is visible                ${sensorlabel4}
    ${SensorLabelText4}     get text                    ${sensorlabel4}
    log                                     ${SensorLabelText4}
    ${sensorlabelvalue4}      get text                    ${sensorvalue4}
    log                                      ${sensorlabelvalue4}
    ${sensorreadingtime}      get text                    ${time}
    log                                      ${sensorreadingtime}

Verify machine detail page has sensor label
     wait until element is visible         ${Dashboardtitle}
     click element                         ${Dashboardtitle}
     #wait until element is visible         ${machinedetailtitle}
     wait until element is visible         ${MDsensorlabel1}
     ${MDsensorlabeltext1}     get text              ${MDsensorlabel1}
     log                                     ${MDsensorlabeltext1}
     ${MDsensorlabeltext2}     get text                    ${MDsensorlabel2}
     log                                     ${MDsensorlabeltext2}
     ${MDsensorlabeltext3}     get text                    ${MDsensorlabel3}
     log                                     ${MDsensorlabeltext3}
     ${MDsensorlabeltext4}     get text                    ${MDsensorlabel4}
     log                                     ${MDsensorlabeltext4}

Verify latest readings is displayed in the get current readings
       wait until element is visible         ${Dashboardtitle}
       click element                         ${Dashboardtitle}
       wait until element is visible            ${Sensorreadingslabel}
       ${Sensorreading}     get text            ${Sensorreadingslabel}
        log                 ${Sensorreading}
