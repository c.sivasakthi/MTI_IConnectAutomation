*** Settings ***
Documentation    iConnect application front end automation test cases
Library          Selenium2Library
Library          DataReader.py
Library          AutoItLibrary
Resource         Login_Resource.robot
Resource         Dashboard_Resource.robot
Resource         Alert_Config_Resource.robot
Resource         Map_Resource.robot
Resource         Reports_Resource.robot

Suite Setup         Run Keywords
...                 Open Browser to Login page
...                 maximize browser window
Suite Teardown      Run Keywords
...                 close browser

*** Test Cases ***
TS#1 Login verification
    [Documentation]     Verifying login scenario's with valid and invalid inputs
    [Tags]      Login verification      Login       Dashboard
    Login_Resource.Login with valid user credential
    Login_Resource.Login with invalid user credential (valid username and empty password and empty username & valid password)
    Login_Resource.Login with invalid user credential (for complete code coverage)

TS#2 Sensor and Status filter button click and verification
    [Documentation]   Verifying filter (Sensor & Status) operation by onclick event
    [Tags]            Filter        Dashboard
    Login_Resource.Login to the iconnect application
    Dashboard_Resource.Verify the dashboard label display
    Dashboard_Resource.Verify the Sensor and Status filter labels in the dashboard
    Dashboard_Resource.Verify Vibration filter and check sensors are getting filtered
    Dashboard_Resource.Click on Temperature button and check sensors are getting filtered
    #Dashboard_Resource.Click on status filters and check machines are getting filtered
    Login_Resource.Logout the iconnect application

TS#3 Sensorlabel in Dashboard
     [Tags]  Dashboard
     Login_Resource.Login to the iconnect application
     Dashboard_Resource.Verify the dashboard has sensor labeland values for each sensor
     Login_Resource.Logout the iconnect application

TS#4 Sensorlabel in MachineDetailpage
     [Tags]  Dashboard
     Login_Resource.Login to the iconnect application
     Dashboard_Resource.Verify machine detail page has sensor label
     Login_Resource.Logout the iconnect application

TS#5 Machine details display in the dashboard
    [Documentation]     Check the machine details in the dashboard and get the current and history records in log
    [Tags]              Dashboard
     Login_Resource.Login to the iconnect application
    Dashboard_Resource.Check the machine names in the dashboard
    Dashboard_Resource.Check the dashboard displayed with current records of temperature and vibration
    Dashboard_Resource.Click on history button and check the records display
    Login_Resource.Logout the iconnect application

TS#6 Machine details in machine detail page
    [Documentation]     Check the machine details in machine detail page
    [Tags]              Dashboard
     Login_Resource.Login to the iconnect application
    Dashboard_Resource.Verify by clicking device link and it navigates to machine detail page
    Dashboard_Resource.verify the machine details are displayed
    Login_Resource.Logout the iconnect application

TS#7 Getcurrentreadingsverification
       [Tags]  IConnectphase2       Dashboard
       Login_Resource.Login to the iconnect application
       Dashboard_Resource.Verify latest readings is displayed in the get current readings
        Login_Resource.Logout the iconnect application

TS#8 Chart page display in Dashboard for the configured machine
    [Documentation]     Check the Graph page display for the configured machine (Vibration and Temperature)
    [Tags]              Dashboard
    Login_Resource.Login to the iconnect application
    Dashboard_Resource.Verify by clicking device link and it navigates to machine detail page
    Dashboard_Resource.Verify the machine details in the graph page
    Dashboard_Resource.Verify datepicker in graph window and check the pop-up window display
    Dashboard_Resource.Click on cancel button and check datepicker window get closed
    Dashboard_Resource.Verify sensor chart display
    Login_Resource.Logout the iconnect application

TS#9 Alert message dislay in Dashboard for the configured machine
    [Documentation]     Check the Graph page display for the configured machine (Vibration and Temperature)
    [Tags]              Dashboard
    Login_Resource.Login to the iconnect application
    Dashboard_Resource.Verify by clicking device link and it navigates to machine detail page
    Dashboard_Resource.Verify the machine details in the graph page
    Dashboard_Resource.Verify by clicking alert link in machine detail page
     Login_Resource.Logout the iconnect application

TC#10 Map page navigation
    [Documentation]     Perform Map page navigation and zoom in and zoom out scenario
    [Tags]              Functional          Map  Test
    Login_Resource.Login to the iconnect application
    Map_Resource.Click on MAP menu header
    Map_Resource.Check the map page display and click on Zoom in and zoom out buttons
    Login_Resource.Logout the iconnect application

TS#11 Map Page Navigation and Map Zoom in, Zoom out Verification
    [Tags]      Maps        Test
    Login_Resource.Login to the iconnect application
    Map_Resource.Click on MAP menu header
    Map_Resource.Check the map page display and click on Zoom in and zoom out buttons
    Login_Resource.Logout the iconnect application

TS#12 Reports Page Label
    [Tags]      Reports     Test
    Login_Resource.Login to the iconnect application
    Reports_Resource.Click on REPORTS menu header
    Reports_Resource.Verify the Reports page has Label for each button
    Login_Resource.Logout the iconnect application

TS#13 Export option in New Reports
    [Tags]      Reports     Test
    Login_Resource.Login to the iconnect application
    Reports_Resource.Click on REPORTS menu header
    Reports_Resource.Verify the Export option in Saved Reports by selecting Date and Sensors
    Login_Resource.Logout the iconnect application

TS#14 Export option in Saved Reports
    [Tags]      Reports         Test
    Login_Resource.Login to the iconnect application
    Reports_Resource.Click on REPORTS menu header
    Reports_Resource.Verify the Export option in New Reports by selecting Date and Sensors
    Login_Resource.Logout the iconnect application

TS#15 Verify the Export option in New Reports without Selecting Sensors
    [Tags]      Reports         Test
    Login_Resource.Login to the iconnect application
    Reports_Resource.Click on REPORTS menu header
    Reports_Resource.Verify the Export option in New Reports without Selecting Sensors
    Login_Resource.Logout the iconnect application

TS#16 verify the Export option in Saved Report without Selecting Sensors
     [Tags]      Reports        Test
    Login_Resource.Login to the iconnect application
    Reports_Resource.Click on REPORTS menu header
    Reports_Resource.verify the Export option in Saved Report without Selecting Sensors
    Login_Resource.Logout the iconnect application

TS#17 Navigate to alert log and configuration page and check alert log and summary page display
    [Documentation]     Perform Alert logs and configuration page navigation check
    [Tags]              Functional          Alert
    Login_Resource.Login to the iconnect application
    Alert_Config_Resource.Click on Alerts dropdown in the menu header
    Alert_Config_Resource.Click on Alert configuration link and check the Alert messages page display
    Alert_Config_Resource.Check the alert logs and configuration field display
    Alert_Config_Resource.Expand the orderby date icon for log display
    Alert_Config_Resource.Click on alert configuration link and check Alert Configuration Summary page display
     Login_Resource.Logout the iconnect application

TC#18 Navigate to alert configuration summary page and perform the CRUD operation for alert
    [Documentation]     Perform alert creation in Alert Configuration page
    [Tags]              Functional          AlertConfiguration
    [Setup]             run keywords
    ...                 login to the iconnect application
   ...                 Click on Alerts dropdown in the menu header
    ...                 Click on Alert Logs link and check the Alert messages page display
    ...                 Click on alert configuration link and check Alert Configuration Summary page display
    Login_Resource.Login to the iconnect application
    Alert_Config_Resource.Click on Add New link in the alert configuration summary page
    Alert_Config_Resource.Click on any one of the device id in left side of the page
    Alert_Config_Resource.Click on Temperature field and add an alert
    Alert_Config_Resource.Enter the details to create an alert in Temperature alert creation page for Device id - 19442
    Login_Resource.Logout the iconnect application

TC#19 Delete the alert in Alert configuration page
    [Documentation]     Perform alert delete in Alert Configuration page
    [Tags]              Functional          Alert Configuration
#    [Setup]             run keywords
#    ...                 login to the iconnect application
#    ...                 Click on Alerts dropdown in the menu header
#    ...                 Click on Alert Logs link and check the Alert messages page display
#    ...                 Click on alert configuration link and check Alert Configuration Summary page display
    Login_Resource.Login to the iconnect application
    Alert_Config_Resource.Click on Delete icon in the Alert Configuration Summary page
    Alert_Config_Resource.Check the confirmation window display and get the alert text
    Alert_Config_Resource.Click on yes button and delete the alert
   Login_Resource.Logout the iconnect application