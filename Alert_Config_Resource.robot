*** Settings ***

Documentation    iConnect Alert feature add and delete scenario
Library          Selenium2Library
Library          String
Library          Collections
Library          DataReader.py

*** Variables ***

${Menu}                                   id=sub_menu_1+1
${alertconfigurationdropdownmenu}         id=submenu_dropdown_$index+1
${Alert_Drp_Dwn}            xpath=/html/body/div[1]/div/ng-include/div/ul/li[4]/a
${Alertconfiguration}       xpath=/html/body/div[2]/ng-view/div/div[1]/ul/li[2]/a
${Alert_Config}             xpath=/html/body/div[2]/ng-view/div/div[3]/div/div[1]/div/h4
#.//*[@id='menu']/ul/li[2]/a
${Alert_Log_Table}          id=center
${Date_Sortby_icon}         xpath=.//*[@id='center']/div/div[4]/div[2]/div/div/div/span/span[1]/i
${Alert_Config_Summary}     xpath=html/body/ng-view/div/div[1]/ul/li[2]/a
${Alert_Summary_Tb}         id=sample_1
# Alert - CRUD
${Alert_Add_New_Link}       xpath=/html/body/ng-view/div/div[3]/div/div/div/h4/span/span[2]/a
${Device_List}              id=devicelist
${Device_182}               xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[1]/div/ul[3]/li/a
${Device_182_Vibration}     xpath=.//*[@id='19442']/li[1]/a
${Device_182_Temperature}   xpath=.//*[@id='19442']/li[2]/a
${Device_182_ID}            id=19442
${182_Alert_Create_Div}     xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div
${Alert_Excel_Path}         Alert_Create.xlsx
${Alert_sheet_name}         AlertCreate
${Alert_Name_Txt}           id=alertname
${Alert_MSG_Txt}            id=alertmessage
${Less_MSG_Link}            xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[2]/label/span
${Mobile_No_1}              xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/input
${ATT_Drop_Down}            xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/select/option[4]
${Less_Mobile_Add}          xpath=html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[1]
${Less_Mobile_Close}        xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[2]
${Less_Email_Link}          xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[3]/label
${Less_Email_Text}          xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/input
${Less_Email_Add}           xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[1]
${Less_Email_Close}         xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/thead/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[2]
${High_MSG_Link}            xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[2]/label/span
${Mobile_No_2}              xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/input
${Verizon_Drop_Down}        xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/select/option[15]
${High_Mobile_Add}          xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[1]
${High_Mobile_Close}        xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[2]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[2]
${High_Email_Link}          xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[3]/label
${High_Email_Text}          xpath=html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[1]/input
${High_Email_Add}           xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[1]
${High_Email_Close}         xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[3]/div/table/tbody/tr/td[3]/label/div/div[2]/div/form/div/div/div/div[1]/div[2]/a[2]
${Slider_Low}               xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[2]/div/span/table/tbody/tr/td/div[2]
${Alert_Create_Save}        xpath=/html/body/ng-view/div/div[3]/div/div[2]/div[2]/div/div[2]/form/div[5]/div/button[1]
# Alert Delete
${Alert_Delete_Icon}        xpath=/html/body/ng-view/div/div[3]/div/div/table/tbody/tr[1]/td[7]/button[2]
${Get_Text_Path}            xpath=/html/body/div[4]/div/div/div[2]
${Alert_Delete_Btn}         xpath=/html/body/div[4]/div/div/div[3]/button[1]
${alerts}                   xpath=//*[@id="sample_1"]
${dashboardtext}        xpath=//*[@id="dashboard"]/div[1]/div[1]/div[1]/div/div[1]
*** Keywords ***

Click on Alerts dropdown in the menu header
    wait until page contains element            ${dashboardtext}
    wait until page contains element            ${dashboardtext}
    wait until page contains                    Dashboard
    sleep       5s
    wait until page contains element            ${Menu}
    click element                               ${Menu}
    wait until page contains element            ${alertconfigurationdropdownmenu}
    click element                               ${alertconfigurationdropdownmenu}
    wait until page contains                       Alert Configuration
    wait until page contains element            ${Alertconfiguration}

Check the alert logs and configuration field display
    wait until page contains                    Alert Configuration
    ${AlertLog-Text}        get text            ${Alertconfiguration}
    log                                         ${AlertLog-Text}
    should be equal                             ${AlertLog-Text}                  Alert Configuration
    wait until page contains                      Alert Configuration Summary
    ${AlertConfig-Text}      get text           ${Alert_Config}
    wait until keyword succeeds
    log                                         ${AlertConfig-Text}


Click on Alert configuration link and check the Alert messages page display
    wait until page contains element                    ${alerts}
    ${alertconfigurationdetails}        get text         ${alerts}
    log                  ${alertconfigurationdetails}

Expand the orderby date icon for log display

    wait until page contains element        ${Date_Sortby_icon}
    double click element                    ${Date_Sortby_icon}

Click on alert configuration link and check Alert Configuration Summary page display

    click element                           ${Alert_Config_Summary}
    wait until page contains element        ${Alert_Summary_Tb}
    page should contain                     Alert Configuration Summary

# Alert - CRUD
Click on Add New link in the alert configuration summary page

    wait until page contains element        ${Alert_Add_New_Link}
    click element                           ${Alert_Add_New_Link}
    wait until page contains element        ${Device_List}

Click on any one of the device id in left side of the page

    wait until page contains element        ${Device_182}
    click element                           ${Device_182}
    wait until page contains                Vibration
    wait until page contains                Temperature
    wait until page contains element        ${Device_182_ID}
    ${Device_Vib}       get text            ${Device_182_Vibration}
    log                                     ${Device_Vib}
    should not be empty                     ${Device_Vib}
    ${Device_Temp}       get text           ${Device_182_Temperature}
    log                                     ${Device_Temp}
    should not be empty                     ${Device_Temp}

Click on Temperature field and add an alert

    wait until page contains element        ${Device_182_Temperature}
    click element                           ${Device_182_Temperature}
    wait until page contains element        ${182_Alert_Create_Div}

Enter the details to create an alert in Temperature alert creation page for Device id - 19442

    ${data}                 getDataFromSpreadsheet    ${Alert_Excel_Path}    ${Alert_sheet_name}
    Set Test Variable       ${test}                   ${data}
    :FOR    ${Name}    ${Message}  ${Mobile_1}  ${Email_1}  ${Mobile_2}  ${Email_2}     IN  @{test}
    #Login with valid username and password
    \       input text              ${Alert_Name_Txt}        ${Name}
    \       input text              ${Alert_MSG_Txt}         ${Message}
    \       click element           ${Less_MSG_Link}
    \       input text              ${Mobile_No_1}           ${Mobile_1}
    \       wait until page contains element                 ${ATT_Drop_Down}
    \       click element           ${ATT_Drop_Down}
    \       wait until page contains element                ${Less_Mobile_Add}
    \       click element                                   ${Less_Mobile_Add}
    \       wait until page contains element                ${Less_Mobile_Close}
    \       click element                                   ${Less_Mobile_Close}
    \       click element                                   ${Less_Email_Link}
    \       input text             ${Less_Email_Text}       ${Email_1}
    \       wait until page contains element                ${Less_Email_Add}
    \       click element                                   ${Less_Email_Add}
    \       click element                                   ${Less_Email_Close}
    \       wait until page contains element                ${High_MSG_Link}
    \       click element                                   ${High_MSG_Link}
    \       input text              ${Mobile_No_2}          ${Mobile_2}
    \       wait until page contains element                ${Verizon_Drop_Down}
    \       click element           ${Verizon_Drop_Down}
    \       click element           ${High_Mobile_Add}
    \       click element           ${High_Mobile_Close}
    \       wait until page contains element                ${High_Email_Link}
    \       click element                                   ${High_Email_Link}
    \       input text             ${High_Email_Text}       ${Email_2}
    \       wait until page contains element                ${High_Email_Add}
    \       click element                                   ${High_Email_Add}
    \       click element                                   ${High_Email_Close}
    \       wait until page contains element                ${Alert_Create_Save}
    \       click element                                   ${Alert_Create_Save}
    \       wait until page contains                        Alert Configuration Summary
    \      exit for loop

# Alert Delete
Click on Delete icon in the Alert Configuration Summary page
     wait until page contains                Alert Configuration Summary
    wait until page contains element        ${Alert_Delete_Icon}
    click element                           ${Alert_Delete_Icon}

Check the confirmation window display and get the alert text

    wait until page contains                 Confirmation
    ${Alert_Txt}   get text                  ${Get_Text_Path}
    log                                      ${Alert_Txt}

Click on yes button and delete the alert

    wait until page contains element        ${Alert_Delete_Btn}
    click element                           ${Alert_Delete_Btn}