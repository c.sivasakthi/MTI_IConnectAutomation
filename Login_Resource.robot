*** Settings ***
Documentation    iConnect login and logout check with valid and invalid credential
Library          Selenium2Library
Library          String
Library          Collections
Library          DataReader.py

*** Variables ***
${LOGIN URL}                http://74.205.92.173:3000/
${BROWSER}                  googlechrome
${Uname_Path}               xpath=/html/body/div[2]/ng-view/div/div[2]/form/div[1]/div/input
${Pwd_Path}                 xpath=/html/body/div[2]/ng-view/div/div[2]/form/div[2]/div/input
${Login_Btn}                xpath=/html/body/div[2]/ng-view/div/div[2]/form/div[4]/button[1]
${Logout_Btn}               xpath=/html/body/div[1]/div/div/ul/li/a
${Login_err_Msg_Path}       xpath=/html/body/div[2]/ng-view/div/div[2]/form/div[3]
${Err_Txt}                  User does not exist in the system.
${Filepath}                 login_testdata.xlsx
${login_sheet_name}         login
${login_invalid}            login_invalid
${invalid_sheetname}        Code-coverage
${Logout_Button}        id=id-logout

*** Keywords ***
Open Browser to Login page
    Open Browser        ${LOGIN URL}        ${BROWSER}
Login with valid user credential
    ${data}                 getDataFromSpreadsheet    ${Filepath}    ${login_sheet_name}
    Set Test Variable       ${test}                   ${data}
    :FOR    ${Username}    ${Password}   IN  @{test}
    #Login with valid username and password
    \       input text          ${Uname_Path}       ${Username}
    \       input text          ${Pwd_Path}         ${Password}
    \       click element       ${Login_Btn}
    \       sleep               2s
    \       wait until page contains element        ${Logout_Btn}
    \       click element       ${Logout_Btn}
    \       reload page
     \      exit for loop
Login to the iconnect application
    ${data}                 getDataFromSpreadsheet    ${Filepath}    ${login_sheet_name}
    Set Test Variable       ${test}                   ${data}
    :FOR    ${Username}    ${Password}   IN  @{test}
    #Login with valid username and password
    \       input text          ${Uname_Path}       ${Username}
    \       input text          ${Pwd_Path}         ${Password}
    \       click element       ${Login_Btn}

Login with invalid user credential (valid username and empty password and empty username & valid password)
    ${data}                 getDataFromSpreadsheet    ${Filepath}    ${login_invalid}
    Set Test Variable       ${test}                   ${data}
    :FOR    ${Invalid_Uname}    ${Invalid_Pwd}   IN  @{test}
    \       input text          ${Uname_Path}       ${Invalid_Uname}
    \       input text          ${Pwd_Path}         ${Invalid_Pwd}
    \       click element       ${Login_Btn}
    \       sleep               2s
    \       ${Text}     get text                    ${Login_err_Msg_Path}
    \       log                                     ${Text}
    \       reload page
Login with invalid user credential (for complete code coverage)
    ${data}                 getDataFromSpreadsheet    ${Filepath}    ${invalid_sheetname}
    Set Test Variable       ${test}                   ${data}
    :FOR    ${Uname}    ${Pwd}   IN  @{test}
    \       input text          ${Uname_Path}       ${Uname}
    \       input text          ${Pwd_Path}         ${Pwd}
    \       click element       ${Login_Btn}
    \       sleep               2s
    \       ${Text}     get text                    ${Login_err_Msg_Path}
    \       log                                     ${Text}
    \       reload page

Logout the iconnect application
    wait until page contains element        ${Logout_Button}
    click element                           ${Logout_Button}